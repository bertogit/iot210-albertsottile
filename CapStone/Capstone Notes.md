# Capstone Project Notes
Create a wireless network of devices each with an api to a piece of equipment.
The makings of a home automation system.  This system included (2) ESP8266 and (1) RPI.

#### Background
* Interested in using the ESP8266 12N WiFi module.

![](ESP8266 12-N Pinout.png?raw=true)
* It has a small amount of memory (~1MB)
* It can be flashed with micropython and a command prompt lets one run custom micropython scripts.
* Micropython libraries are similar to python.  There are fewer available libraries.  Micropython equivalents begin with a "u" (as in micro).
* Interested in RS232 serial control via UART ports of a raspberry pi.
* Idea to create a wireless network of devices that each have an api, and a central raspberry pi to send requests out to each device as needed.
* Chose to connect to RS232 devices but concept could be applied to relays and I/O ports as well.
* There are several versions of ESP8266 modules and prices are between $10-$20.  This makes for an affordable solution to creating a wireless network of devices.

#### Tutorials and Useful Sites
* Basically anything by Tony Dicola with Adafruit
> * https://learn.adafruit.com/micropython-basics-load-files-and-run-code
> * https://learn.adafruit.com/micropython-basics-how-to-load-micropython-on-a-board
> * https://learn.adafruit.com/micropython-basics-what-is-micropython
> * https://learn.adafruit.com/building-and-running-micropython-on-the-esp8266/flash-firmware
> * https://learn.adafruit.com/micropython-basics-loading-modules/frozen-modules
> * https://learn.adafruit.com/building-and-running-micropython-on-the-esp8266/build-firmware

* Micropython.org
> * https://docs.micropython.org/en/latest/esp8266/esp8266/tutorial/index.html

* Micropython on GitHub
> * https://github.com/micropython/micropython
> * https://github.com/micropython/micropython-lib

* MicropPython Forum - **pfalcon, pythoncoder, damien**
> * https://forum.micropython.org/viewforum.php?f=16

* Credit to Micropython's creator
> * http://dpgeorge.net/
> * https://github.com/dpgeorge

* **uPyLoader-win.exe** - a gem for working with ESP8266!
> * https://github.com/BetaRavener/uPyLoader/releases

* **ampy** - commadn utlity for working with ESP8266
> * https://learn.adafruit.com/micropython-basics-load-files-and-run-code/install-ampy

* **NodeMCU** - firmware ESP8266Flasher
> * https://nodemcu.readthedocs.io/en/master/en/flash/


#### Process
* The Basic:
  ```
  $pi: pip install esptool
  $pi: esptool.py --port YOURCOMPORT erase_flash
  ```
  * Nodemcu Firmware flasher runs on windows (ESP8266Flasher.exe)
  * get from https://micropython.org/download
  * Successful firmware version "esp8266-20170108-v1.8.7.bin"
  * connect via putty serial port; usb will report as serial port
  * look for >>> prompt; micropython is ready


* Configure network settings (in the esp8266) "the station" and "the access point":
```python
import network
sta=network.WLAN(network.STA_IF)
ap=network.WLAN(network.AP_IF)
sta.active(True)
ap.active(True)      		
sta.connect('ssid', 'password')
ap.config(essid='ssid')
ap.config(password='password')
sta.ifconfig()
ap.ifconfig()
```


* These settings can be added to the boot.py file that runs on the ESP8266 when it first starts.
* After boot.py, main.py will run
* very simple commands should go in boot.py, while your primary script goes in main.py.
* main.py can call other scripts.
* all scripts reside in the same root directory


* The ESP8266 micropython runs in a loop called the REPL (read, evaluate, process, loop).
* There is also a webREPL version which works well once the network settings are configured correctly.
https://docs.micropython.org/en/latest/esp8266/esp8266/tutorial/repl.html#webrepl-a-prompt-over-wifi


* use ampy tool to move files get and put files to esp8266
* example: put the boot.py file on esp8266: ```ampy --port YOURCOMPORT put boot.py```
* review ampy on github for full api


* There are many tools for working with the esp8266 like esptools.py, ampy.py, nodemcu.exe, webrepl and putty.  It is difficult to move between them especially if one is changing the baud rate on the UART port, which is the primary port of communication.
* After much struggling with this, I came across **uPyLoader-win.exe** which encapsulated many of these features.  It is recommended to use this instead of the several different tools for the different tasks necessary.  

#### The Code
* ESP8266 CODE: each esp8266 gets boot.py, uartport.py and main.py
  * *boot.py* - my version of the boot file
  * *config.py* - the configuration of a new esp8266; run locally from laptop directed at esp8266.
  * *uartport.py* - class for handling uart port transmit and receive strings.
  * *swt/main.py* - the api for an audio video switcher; contains routes for "input" selection.
  * *display/main.py* - the api for a display TV ; contains routes for "input" selection and "power" state.


* MASTER CODE: runs on the raspberry pi
  * *master.py* - contains the primary api called by a client; contains routes for "macros" that send requests to the other esp8266 as needed.


#### Learned
* There was no flask server for micropython, but picoweb was "flask-like".  required freezing into micropython build.  This required spinning up a vagrant VM (adafruit link above).  The picoweb package was then copied into the required folder and a new fimware was built that included picoweb. There is a package installer (upip) in micropython but picoweb was too big for the remaining memory.
* The only bi directional UART port (UART 0) was also outputting data from the ESP8266.  There was another TX only UART (UART1) that had no traffic. Since the goal was to use the UART ports for RS232, the decision was made to use the RX of UART0 and the TX of UART1.
