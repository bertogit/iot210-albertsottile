# green
# control Extron switcher

import ujson
import uio
import picoweb
from uartport import UARTport

app = picoweb.WebApp(__name__)
uport = UARTport()

@app.route('/control', methods=['GET', 'PUT', 'POST', 'DELETE'])
def control(req, resp):
    yield from picoweb.start_response(resp)
    data = req.qs.split("=")
    yield from resp.awrite("request " + data[0] + " : "  + data[1] + " , ")

    if data[0] == "input":
        strTX = data[1] + "!" + chr(13)

    yield from resp.awrite(uport.sendTX(strTX))

if __name__ == "__main__":
    print("Main")
    uport.run()
    app.run(debug=False, host = "0.0.0.0", port=5000)
    #app.run(debug=False, host = "0.0.0.0", port=5000, log=None)
