import uasyncio as asyncio
from machine import UART

class uloop:
    async def getRX(self, rx):
        while True:
            tmp = rx.read()
            if tmp != None:
                print(tmp.decode("utf-8"))
            await asyncio.sleep(1)

class UARTport(object):
    def __init__(self):
        self.rx = UART(0, 9600)
        self.tx = UART(1, 9600)
        self.rx.init(9600, bits=8, parity=None, stop=1)

    def sendTX(self, str):
        self.tx.write(str)
        return "sent rs232 cmd \"" + str + "\""

    def run(self):
        asyncio.ensure_future(uloop().getRX(self.rx), asyncio.get_event_loop())
        print('run loop')
