#!/usr/bin/python
# Albert Sottile - 2018/3/05

import json
import io
from flask import Flask, request
import requests

# ================= CONSTANTS ==========================
HTTP_OK = 200
HTTP_NF = 404
PORT = 5000

#swt = 'http://192.168.86.176:5000/control'
#display = 'http://192.168.86.180:5000/control'
swt = 'http://192.168.1.102:5000/control'
display = 'http://192.168.1.103:5000/control'

# ================= VARIABLES ==========================

# ================= FUNCTIONS ==========================

# ================= API ================================

app = Flask(__name__)

@app.route('/', methods=['GET', 'PUT', 'POST', 'DELETE'])
def index():
    rsp_data = "hello"
    return rsp_data, HTTP_OK

@app.route('/control/macro/<macro>', methods=['GET', 'PUT', 'POST', 'DELETE'])
def macro(macro):
    rsp_data = ""
    if macro == "0":    # OFF
        rsp_data += "swt " + requests.post(swt, params = {"input": "0"}).text + " | "
        rsp_data += "display " + requests.post(display, params = {"power": "off"}).text + " | "

    if macro == "1":    # INPUT 1
        rsp_data += "swt " + requests.post(swt, params = {"input": "1"}).text + " | "
        rsp_data += "display " + requests.post(display, params = {"power": "on"}).text + " | "

    elif macro == "2":  # INPUT 2
        rsp_data += "swt " + requests.post(swt, params = {"input": "2"}).text + " | "
        rsp_data += "display " + requests.post(display, params = {"power": "on"}).text + " | "

    return rsp_data, HTTP_OK





# ============================== MAIN ====================================

if __name__ == "__main__":
  print "Master Controller"
  app.run(host='0.0.0.0', port=PORT, debug=True, use_reloader=False)
