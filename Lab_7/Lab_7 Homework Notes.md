# homework notes
Convert your Rapsberry Pi into a Bluetooth Low Energy (BLE) Speaker

* followed instructions from this link:  https://www.raspberrypi.org/forums/viewtopic.php?t=161944
* typo in Step-4 : "edit the ~/.bashrc"

* had to build and install bluez : https://learn.adafruit.com/install-bluez-on-the-raspberry-pi/installation

* had to find **bluezutils.py**
https://github.com/pauloborges/bluez/blob/master/test/bluezutils.py

* issue: "Daemon startup failed" however below did not fix it
https://raspberrypi.stackexchange.com/questions/50309/pulseaudio-not-autostarting-on-pi-3/50326#50326

* *End Result* : not playing audio through 3.5mm, but connecting to pi bluetooth. RPI desktop shows pulse audio is working correctly, that my bluetooth device is connected and that it is configured to playback music. Confirmed the pi plays audio files originating from within, just not bluetooth streaming.
