#!/usr/bin/python
# Albert Sottile - 2018/01/13

import json
import io
import atexit


class ToDoList(object):

    def __init__(self):
        self.todolist = self.importList()

    def __del__(self):
        self.exportList()

    def importList(self):
        jdata = []
        try:
            with open('todolist.json') as f:
                jdata = json.load(f)
        except Exception as e:
            jdata = []
        return jdata

    def getList(self):
        tmp = "ToDoList:\n"
        try:
            for i in self.todolist:
                print(i)
                tmp += i['item'] + ' = ' + str(i['complete']) + '\n'
        except Exception as e:
            raise
        return tmp

    def addToList(self,item):
        self.todolist.append(item)
        return "added " + str(item)

    def updateList(self,item):
        status = "not found "
        if item['item'].isdigit() and int(item['item']) < len(self.todolist):
            self.todolist[int(item['item'])]['complete'] = item['complete']
            item['item']=self.todolist[int( item['item'])]['item']
            status = "updated "
        else:
            n=0
            for i in self.todolist:
                if i['item'] == item['item']:
                    self.todolist[n]=item
                status = "updated "
                pass
                n+=1
        return status + str(item)

    def getItemByNum(self,num):
        return self.todolist[num]

    def delFromList(self,item):
        status = "not found "
        if item['item'].isdigit() and int(item['item']) < len(self.todolist):
            del self.todolist[int(item['item'])]
            status = "deleted "
        else:
            n=0
            for i in self.todolist:
                if i['item'] == item['item']:
                    del self.todolist[n]
                status = "deleted "
                pass
                n+=1
        return status + str(item)

    def exportList(self):
        print "Goodbye!"
        if self.todolist:
            with io.open('todolist.json', 'w', encoding='utf-8') as f:
              f.write(json.dumps(self.todolist, ensure_ascii=False))



# ============================== MAIN ====================================

if __name__ == "__main__":
    print "ToDoList"
    #tdl = ToDoList()
    #tmp = json.loads('[{"item":"Read List","complete":false},{"item":"Check List","complete":false},{"item":"Delete List","complete":false}]')
    #for item in tmp:
    #    tdl.addToList(item)
    #obj = {'item':'Delete List', 'complete':True}
    #tdl.addToList(obj)
    #tdl.updateList(obj)
    #print(tdl.getList())
