#!/usr/bin/python
# Albert Sottile - 2018/01/13

#import random
#import string
import json
import io
#import sys
from flask import Flask, request
from todolist import ToDoList

# ================= CONSTANTS ==========================
HTTP_OK = 200
HTTP_NF = 404
PORT = 5000

# ================= VARIABLES ==========================

# ================= FUNCTIONS ==========================
def sbool(s):       # convert a T/F string to true bool
    if s == 'True' or s == 'true':
         return True
    elif s == 'False' or s == 'false':
         return False

# ============================== API ====================================
# curl examples:
# curl -H 'Content-Type: text/plain' 127.0.0.1:5000/todolist/api
# curl -X PUT -d 'albert' 127.0.0.1:5000/todolist/api
# curl -X PUT -H "Content-Type: multipart/form-data;" -F "key1=val1" 127.0.0.1:5000/todolist/api
# curl -X PUT -H "Content-Type: application/json" -d '{"key1":"value"}' 127.0.0.1:5000/todolist/api
# curl -X POST 127.0.0.1:5000/todolist/api -F 'file=@/file-path.json'
# ~~~~~~~~~~~~~~~~~~~~~~ USE FOR TESTING ~~~~~~~~~~~~~~~~~~~~~
# curl -X POST 127.0.0.1:5000/todolist/api -H "Content-Type: application/json" -d '[{"item":"Read List","complete":false},{"item":"Check List","complete":false},{"item":"Delete List","complete":false}]'
# curl -X POST -H "Content-Type: application/json" -d '[{"item":"Do Something","complete":false}]' 127.0.0.1:5000/todolist/api
# curl -X PUT "127.0.0.1:5000/todolist/api?item=1&complete=true"
# curl -X PUT "127.0.0.1:5000/todolist/api?item=Read%20List&complete=true"
# curl -X DELETE "127.0.0.1:5000/todolist/api?item=1"
# curl -X DELETE "127.0.0.1:5000/todolist/api?item=Do%20Something"

# create the global objects
app = Flask(__name__)
tdlist = ToDoList()

@app.route("/todolist/api", methods=['GET', 'PUT', 'POST', 'DELETE'])
def API():
  rsp_data = "";

  # Get the entire list
  # EXAMPLE: curl -H 'Content-Type: text/plain' 127.0.0.1:5000/todolist/api
  if request.method == 'GET':
      rsp_data += "GET \n"
      rsp_data += tdlist.getList()

  # Update "completed" status of an item in the list; can be entered by item name or item num
  # EXAMPLE: curl -X PUT "127.0.0.1:5000/todolist/api?item=1&complete=true"
  elif request.method == 'PUT':
      rsp_data += "PUT \n"
      string = '{"item":"' + str(request.args['item']) + '", "complete":' + str(request.args['complete']) + '}'
      obj = json.loads(string)
      print(tdlist.updateList(obj))
      rsp_data += tdlist.getList()

  # Create a new item; append to end of list array
  # EXAMPLE: curl -X POST -H "Content-Type: application/json" -d '[{"item":"Do Something","complete":false}]' 127.0.0.1:5000/todolist/api
  elif request.method == 'POST':
      rsp_data += "POST\n"
      if request.is_json:
          jdata = json.loads(request.get_data())
          for item in jdata:
              print(tdlist.addToList(item))
          rsp_data += tdlist.getList()

  # Remove an item from the list; can be entered by item name or item number
  # EXAMPLE: curl -X DELETE "127.0.0.1:5000/todolist/api?item=1"
  elif request.method == 'DELETE':
      rsp_data += "DELETE\n"
      string = '{"item":"' + str(request.args['item']) + '"}'
      obj = json.loads(string)
      print(tdlist.delFromList(obj))
      rsp_data += tdlist.getList()


  return rsp_data, HTTP_OK




# ============================== MAIN ====================================

if __name__ == "__main__":
  print "ToDoList Server"
  app.debug = True
  app.run(host='0.0.0.0', port=PORT)
