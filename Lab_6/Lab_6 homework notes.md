# homework notes
Here are the steps I performed to create a Robot Thing:


* create a robot type
```
aws iot create-thing-type --thing-type-name "RobotType" --thing-type-properties "thingTypeDescription=robot type, searchableAttributes=name,purpose,degrees-separation-kevin-bacon"
```

* create a robot thing
```
aws iot create-thing --thing-name "LAB6-Robot" --thing-type-name "RobotType" --attribute-payload "{\"attributes\": {\"name\":\"robot\",\"purpose\":\"nothing\",\"description\":\"iotrobot\",\"picture\":\"root\",\"location\":\"somewhere\",\"degrees-separation-kevin-bacon\":\"7\"}}"
```

* list the thing by type
```
aws iot list-things --thing-type-name "RobotType"
```

* create public key, private key and cert pem files
```
aws iot create-keys-and-certificate --set-as-active --certificate-pem-outfile cert.pem --public-key-outfile publicKey.pem --private-key-outfile privkey.pem
```

* create a policy for client access; the iotpolicy.json file contains permissions to all iot actions
```
aws iot create-policy --policy-name "PubSubToAnyTopic" --policy-document file://iotpolicy.json
```

* attach policy (use result from key creation)
```
aws iot attach-principal-policy --principal "your-certificate-arn-here" --policy-name "PubSubToAnyTopic"
```

* attach principal to a thing
```
aws iot attach-thing-principal --thing-name "LAB6-Robot" --principal "your-certificate-arn-here"
```

* add a shadow to a thing
> couldn't figure out how to do this via aws iot sdk in python; had to use web interface to add a shadow.

* send a request to my thing's shadow
```
curl --tlsv1.2 --cert ./cert.pem --key ./privkey.pem --cacert ./awsIotRootCert.crt "https://a3bjpj0f7etdzf.iot.us-west-2.amazonaws.com:8443/things/LAB6-Robot/shadow"
```
* ... and the response
```
{"state":{"desired":{"welcome":"aws-iot"},"reported":{"welcome":"aws-iot"}},"metadata":{"desired":{"welcome":{"timestamp":1521004821}},"reported":{"welcome":{"timestamp":1521004821}}},"version":1,"timestamp":1521062899}
```
___
## QUESTIONS:
- access to my endpoint via the above curl command only shows the state of the shadow. How could I view the json data for my thing (ie the robot atrributes)?
- how would someone else be able to access my thing if all the pem files are on my computer?
- what is the correct way to grant access to a client?
- what is the use of the public key since it only seems necessary to use the attach the private key and the cert?
- is there a security issue with giving out the pem files?  Aren't they only for accessing one thing?
- the mqtt pub sub python programs just repeat the topic and string.  Is there a publish topic and payload that would return the json of my thing?
