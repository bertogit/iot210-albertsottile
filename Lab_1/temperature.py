#!/usr/bin/python
# Albert Sottile
# Displays location temperature from weather underground API.
# To use on bootup, edit the rc.local file and place temperature.py in home/pi directory.
#       sudo nano /etc/rc.local add line: python /home/pi/temperature.py &
#

from sense_hat import SenseHat
from urllib2 import urlopen
import json
import time
import datetime
from threading import Thread
import Queue
import atexit


apikey="655a1c5713680cdb"
url="http://api.wunderground.com/api/"+apikey+"/conditions/q/WA/Seattle.json"

sense = SenseHat()
sense.set_rotation(180)
sense.show_letter('P')

location = ""
tempf = ""

# FUNCTIONS ========================================================
def getInitialData():
    data = urlopen(url).read()
    data = data.decode('utf-8')
    jsdata = json.loads(data)
    return jsdata['current_observation']['display_location']['city'], jsdata['current_observation']['temp_f']

def getTemp(tempf,q):
    while True:
        data = urlopen(url).read()
        data = data.decode('utf-8')
        jsdata = json.loads(data)
        tempf = jsdata['current_observation']['temp_f']
        print "new temperature requested at time", datetime.datetime.now()
        q.put(tempf)
        time.sleep(300)     # request new data every 5 minutes

def showTemp(tempf,q):
    while True:
        if q.empty() != True:
            tempf = q.get()
        msg = "{} {} F".format(location,tempf)
        sense.show_message(msg)
        time.sleep(10)      # show message marque every 10 seconds

def exit_handler():
    print "Goodbye!"
    sense.clear()

# MAIN ============================================================
if __name__ == "__main__":

    # bootup: give time to get on the Wi-Fi network
    time.sleep(10)

    location, tempf = getInitialData()
    q = Queue.Queue()
    q.put(tempf)
    t1 = Thread(target = getTemp, args=(tempf,q))
    t2 = Thread(target = showTemp, args=(tempf,q))
    #t1.setDaemon(True)
    #t2.setDaemon(True)
    t1.start()
    t2.start()

    atexit.register(exit_handler)
    #raw_input()             # keep script running and output window open
