#!/usr/bin/python
# Albert Sottile - 2018/01/23

import json
import io
from flask import Flask, request
from todolist import ToDoList
import xml.etree.ElementTree as ET
import dicttoxml as xml
import atexit

# ================= CONSTANTS ==========================
HTTP_OK = 200
HTTP_NF = 404
PORT = 5000
content_plain     = 'text/plain'
content_json      = 'application/json'
content_xml       = 'application/xml'
content_protobufs = 'application/protobuf'
type_PLAIN    = 0
type_JSON     = 1
type_XML      = 2
type_PROTOBUF = 3

# ================= VARIABLES ==========================

# ================= FUNCTIONS ==========================
def sbool(s):       # convert a T/F string to true bool
    if s == 'True' or s == 'true':
         return True
    elif s == 'False' or s == 'false':
         return False


# ================= EXAMPLE CURL =======================
#__JSON______________________________
# curl -X GET -H 'Content-Type: application/json' '127.0.0.1:5000/todolist/api'
# curl -X PUT -H 'Content-Type: application/json' '127.0.0.1:5000/todolist/api?item=1&complete=true'
# curl -X PUT -H 'Content-Type: application/json' '127.0.0.1:5000/todolist/api' -d '{"name":"List Item A","complete":"True"}'
# curl -X POST -H 'Content-Type: application/json' '127.0.0.1:5000/todolist/api' -d '{"name":"List Item G","complete":"False"}'
# curl -X DELETE -H 'Content-Type: application/json' '127.0.0.1:5000/todolist/api?item=List%20Item%20B'
# curl -X DELETE -H 'Content-Type: application/json' '127.0.0.1:5000/todolist/api' -d '{"name":"List Item G","complete":"True"}'

#__XML______________________________
# curl -X GET -H 'Content-Type: application/xml' '127.0.0.1:5000/todolist/api'
# curl -X PUT -H 'Content-Type: application/xml' '127.0.0.1:5000/todolist/api?item=1&complete=true'
# curl -X PUT -H 'Content-Type: application/xml' '127.0.0.1:5000/todolist/api' -d '<item><name>List Item A</name><complete>True</complete></item>'
# curl -X POST -H 'Content-Type: application/xml' '127.0.0.1:5000/todolist/api' -d '<item><name>List Item G</name><complete>False</complete></item>'
# curl -X DELETE -H 'Content-Type: application/xml' '127.0.0.1:5000/todolist/api' -d '<item><name>List Item G</name><complete>True</complete></item>'


# ================= API ================================

app = Flask(__name__)
tdlist = ToDoList()

@app.route("/todolist/api", methods=['GET', 'PUT', 'POST', 'DELETE'])
def Api():
    rsp_data = "";

    # Get the content_type of request
    content_type = type_PLAIN
    if 'Content-Type' in request.headers:
        # print "Content-Type: " + request.headers['Content-Type']
        if request.headers['Content-Type'] == content_xml:
            content_type = type_XML
        elif request.headers['Content-Type'] == content_protobufs:
            content_type = type_PROTOBUF
        elif request.headers['Content-Type'] == content_json:
            content_type = type_JSON

    # Get the entire list
    if request.method == 'GET':
        rsp_data += tdlist.getList(content_type)

    # Update "completed" status of an item in the list; can be entered by item name or item num
    elif request.method == 'PUT':
        if content_type == type_JSON:
            item = json.loads(request.get_data())
        elif content_type == type_XML:
            item = ET.fromstring(request.get_data())
        print(tdlist.updateList(item,content_type))
        rsp_data += tdlist.getList(content_type)

    # Create a new item; append to end of list array
    elif request.method == 'POST':
        if content_type == type_JSON:
            item = json.loads(request.get_data())
        elif content_type == type_XML:
            item = ET.fromstring(request.get_data())
        print(tdlist.addToList(item,content_type))
        rsp_data += tdlist.getList(content_type)

    # Remove an item from the list; can be entered by item name or item number
    elif request.method == 'DELETE':
        if content_type == type_JSON:
            item = json.loads(request.get_data())
        elif content_type == type_XML:
            item = ET.fromstring(request.get_data())
        print(tdlist.delFromList(item,content_type))
        rsp_data += tdlist.getList(content_type)


    return rsp_data, HTTP_OK




# ============================== MAIN ====================================

if __name__ == "__main__":
  print "ToDoList Server"
  app.run(host='0.0.0.0', port=PORT, debug=True, use_reloader=False)
