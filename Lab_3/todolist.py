#!/usr/bin/python
# Albert Sottile - 2018/01/23

import json
import xml.etree.ElementTree as ET
import dicttoxml as dtox
import io
import atexit


class ToDoList(object):
    type_PLAIN    = 0
    type_JSON     = 1
    type_XML      = 2
    type_PROTOBUF = 3

    def __init__(self):
        self.todolist = self.importList()

    def __del__(self):
        self.exportList()

    def importList(self):
        jdata = []
        try:
            with open('todolist.json') as f:
                jdata = json.load(f)['items']
        except Exception as e:
            jdata = []
        finally:
            f.close
        return jdata

    def exportList(self):
        print "Goodbye!"
        try:
            if self.todolist:
                jstr = '{"items": ' + json.dumps(self.todolist, ensure_ascii=False) + '}'
                with io.open('todolist.json', 'w', encoding='utf-8') as f:
                  f.write(jstr)
        except Exception as e:
            print('file write error')
        finally:
            f.close

    def getList(self,dtype):
        tmp = ""
        try:
            if dtype==self.type_PLAIN:
                for i in self.todolist:
                    tmp += i['name'] + ' = ' + str(i['complete']) + '\n'
            elif dtype==self.type_JSON:
                tmp += json.dumps(self.todolist)
            elif dtype==self.type_XML:
                tmp += dtox.dicttoxml(self.todolist)
        except Exception as e:
            raise
        return tmp + "\n"

    def getItemByNum(self,num):
        return self.todolist[num]

    def createItem(self,name,complete):
        string = '{"name":"' + name + '", "complete":"' + complete + '"}'
        return json.loads(string)

    def addToList(self,item,dtype):
        if dtype==self.type_JSON:
            item = item
        elif dtype==self.type_XML:
            name = item.find('name').text
            complete = item.find('complete').text
            item = self.createItem(name,complete)

        self.todolist.append(item)
        return "added " + str(item)

    def updateList(self,item,dtype):
        status = "not found "
        n = -1
        if dtype==self.type_JSON:
            name = item['name']
        elif dtype==self.type_XML:
            name = item.find('name').text
            complete = item.find('complete').text
            item = self.createItem(name,complete)

        if name.isdigit():
            n=int(name)
        else:
            for num, i in enumerate(self.todolist):
                if i['name'] == name:
                    n = num
                    break

        if n < len(self.todolist) and n > -1:
            self.todolist[n]=item
            status = "updated "
        return status + str(self.todolist[n])

    def delFromList(self,item,dtype):
        status = "not found "
        n = -1
        if dtype==self.type_JSON:
            name = item['name']
        elif dtype==self.type_XML:
            name = item.find('name').text
            complete = item.find('complete').text
            item = self.createItem(name,complete)

        if name.isdigit():
            n=int(name)
        else:
            for num, i in enumerate(self.todolist):
                if i['name'] == name:
                    n = num
                    break

        if n < len(self.todolist) and n > -1:
            del self.todolist[n]
            status = "deleted "
        return status + str(item)


# ============================== MAIN ====================================

if __name__ == "__main__":
    print "ToDoList"
    #tdl = ToDoList()
    #obj = {'name':'List Item D', 'complete':"False"}
    #tdl.addToList(obj)
    #obj = {'name':'List Item D', 'complete':"True"}
    #tdl.updateList(obj)
    #obj = {'name':'List Item D'}
    #tdl.delFromList(obj)
    #print(tdl.getList())
