# homework notes
Converted Pi into an access point and served a secure web server at bootup.

* followed the steps at the following website to setup the pi as an access point: https://thepi.io/how-to-use-your-raspberry-pi-as-a-wireless-access-point/
* this sets the wlan0 as static and creates a bridge between wlan0 and eth0.
* it also sets up a dhcp server on the wlan0 interface.
* I found that after the setting it up, the eth0 was serving IP addresses from my network router through the bridge, and the dhcp server on wlan0 was not doing anything.
* I was however able to connect to the wlan0 and access my network connected to eth0, so the accesspoint was working; it just wasnt serving ip addresses.
* Discussing with classmate, Chris Higbie, the bridge seems to break the wlan0 dhcp server thus opening the eth0 interface to server dhcp over the bridge.
* The purpose of the bridge was to get internet access though, so this was not a good solution.

The following steps allow a script to run at bootup:
* edit the file...
```
sudo nano /etc/rc.local
```

* add the following line before "exit 0"...
```
sudo python /home/pi/apserver.py &
```

# update
* restored my pi back to previous state.
* experimented with ESP8266 running micropython for my Capstone project.
* this allowed me to run the device as an access point and serve a web api at bootup.
* see notes on Capstone project.
